// import a module or call a function
//  const logger= require('./logger');

//  logger.logger('kanishk');

//====================================
// get path function
 
// const path = require('path');
// pathObj= path.parse(__filename);
// console.log(pathObj);


//====================================

// get memory function

// const os= require('os');
// var totalMemory= os.totalmem;
// var freeMemory= os.freemem;

// console.log(totalMemory+' === '+freeMemory);

//====================================
// get file info 

 // const fs= require('fs');
// const files= fs.readdirSync('./');

// fs.readdir('./', function(err,files){
//     if(err) console.log('Error',err);
//     else console.log('Result',files)

// })

//====================================

// EventEmitter events

// const EventEmitter= require('events'); // class

// const Mylogger = require('./logger');
// const logger= new Mylogger();

// // register a listner
// logger.on('messageLogged',function(arg){
//     console.log('Listner Called', arg)

// })

// // Raise an Event
// logger.log('kanishk')


//=======================================

// start server and make connection

const http= require('http');
const server= http.createServer((req,res)=>{

    if(req.url === '/'){
        res.write('Hello World');
        res.end();
    }

    if(req.url == '/api/courses'){

        res.write(JSON.stringify([1,2,3]));
        res.end();

    }

});

// register a listner
// server.on('connection',(socket) => {

//     console.log('New Connection ...');
// });

// raise an event

server.listen(3000);
console.log('Listning on port 3000...');


